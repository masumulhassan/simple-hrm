package me.masum.hrm

import grails.testing.web.interceptor.InterceptorUnitTest
import spock.lang.Specification

class BasicAuthInterceptorSpec extends Specification implements InterceptorUnitTest<BasicAuthInterceptor> {

    def setup() {
    }

    def cleanup() {

    }

    void "Test basicAuth interceptor matching"() {
        when:"A request matches the interceptor"
        withRequest(controller:"basicAuth")

        then:"The interceptor does match"
        interceptor.doesMatch()
    }
}
