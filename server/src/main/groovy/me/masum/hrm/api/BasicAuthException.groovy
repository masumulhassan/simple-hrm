package me.masum.hrm.api

class BasicAuthException extends RuntimeException {
    private String message
    private Integer statusCode = 400
    private List messageParams = []

    public BasicAuthException(String message){
        this.message = message
    }

    public BasicAuthException(String message, List args){
        this.message = message
        this.messageParams = args
    }

    public BasicAuthException(String message, int statusCode){
        this(message)
        this.statusCode = statusCode
    }

    public int getStatusCode() {
        return statusCode
    }

    public List getMessageParams() {
        return messageParams;
    }

    public String getMessage() {
        return this.message
    }
}
