package me.masum.hrm

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import org.grails.datastore.mapping.core.Datastore
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

@Integration
@Rollback
class BasicAuthTokenServiceSpec extends Specification {

    BasicAuthTokenService basicAuthTokenService
    @Autowired Datastore datastore

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BasicAuthToken(...).save(flush: true, failOnError: true)
        //new BasicAuthToken(...).save(flush: true, failOnError: true)
        //BasicAuthToken basicAuthToken = new BasicAuthToken(...).save(flush: true, failOnError: true)
        //new BasicAuthToken(...).save(flush: true, failOnError: true)
        //new BasicAuthToken(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //basicAuthToken.id
    }

    void cleanup() {
        assert false, "TODO: Provide a cleanup implementation if using MongoDB"
    }

    void "test get"() {
        setupData()

        expect:
        basicAuthTokenService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BasicAuthToken> basicAuthTokenList = basicAuthTokenService.list(max: 2, offset: 2)

        then:
        basicAuthTokenList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        basicAuthTokenService.count() == 5
    }

    void "test delete"() {
        Long basicAuthTokenId = setupData()

        expect:
        basicAuthTokenService.count() == 5

        when:
        basicAuthTokenService.delete(basicAuthTokenId)
        datastore.currentSession.flush()

        then:
        basicAuthTokenService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BasicAuthToken basicAuthToken = new BasicAuthToken()
        basicAuthTokenService.save(basicAuthToken)

        then:
        basicAuthToken.id != null
    }
}
