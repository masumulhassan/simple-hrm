package me.masum.hrm

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat

@Transactional
class EmployeeLeaveService {
    EmployeeService employeeService

    Map getEmployeeLeaveDetails(Long employeeId, Long leaveId) {
        Employee employee = employeeService.getEmployee(employeeId)
        if(employee.is(null)){
            return [employeeLeave: null, missing: "Employee", missingId: employeeId]
        } else {
            EmployeeLeave employeeLeave = employee.getLeaves().find {
                it.leaveId == leaveId
            }
            return [employeeLeave: employeeLeave, missing: "Employee leave", missingId: leaveId]
        }
    }

    EmployeeLeave getEmployeeLeave(Long leaveId) {
        return EmployeeLeave.findByLeaveId(leaveId)
    }

    Map saveEmployeeLeave(JSONObject params, Long employeeId) {
        return validateAndSaveEmployeeLeave(params, employeeId, null, false)
    }

    Map updateEmployeeLeave(JSONObject params, Long employeeId, Long employeeLeaveId) {
        return validateAndSaveEmployeeLeave(params, employeeId, employeeLeaveId, false)
    }

    Map patchEmployeeLeave(JSONObject params,Long employeeId, Long employeeLeaveId) {
        return validateAndSaveEmployeeLeave(params, employeeId, employeeLeaveId, true)
    }

    Map deleteEmployeeLeave(Long employeeId, Long employeeLeaveId){
        if(employeeService.getEmployee(employeeId)) {
            EmployeeLeave employeeLeave = getEmployeeLeave(employeeLeaveId)
            if (employeeLeave) {
                employeeLeave.delete()
                return [ isDeleted: true ]
            }
            return [ isDeleted: false, missing: "Leave", missingId: employeeLeaveId ]
        }
        return [ isDeleted: false, missing: "Employee", missingId: employeeId ]
    }

    Map approveLeave(Long employeeId, Long employeeLeaveId){
        return changeStatus(employeeId, employeeLeaveId, "Approved")
    }

    Map rejectLeave(Long employeeId, Long employeeLeaveId){
        return changeStatus(employeeId, employeeLeaveId, "Rejected")
    }

    Map changeStatus(Long employeeId, Long employeeLeaveId, String status){
        if(employeeService.getEmployee(employeeId)) {
            EmployeeLeave employeeLeave = getEmployeeLeave(employeeLeaveId)
            if (employeeLeave) {
                if(employeeLeave.status == "Pending") {
                    employeeLeave.status = status
                }
                employeeLeave.save()
                return [ isStatusChanged: true, employeeLeave: employeeLeave ]
            }
            return [ isStatusChanged: false, missing: "Leave", missingId: employeeLeaveId ]
        }
        return [ isStatusChanged: false, missing: "Employee", missingId: employeeId ]
    }

    private Map validateAndSaveEmployeeLeave(JSONObject params, Long employeeId, Long employeeLeaveId, Boolean patchEmployeeLeave) {
        EmployeeLeave employeeLeave

        if (!patchEmployeeLeave && (employeeId.is(null) || employeeService.getEmployee(employeeId).is(null))) {
            return [ employeeLeave: null, missing: "Employee", missingId: employeeId ]
        }

        if (!employeeLeaveId.is(null)){
            employeeLeave = getEmployeeLeave(employeeLeaveId)
            if (employeeLeave == null) {
                if (patchEmployeeLeave){
                    return [ employeeLeave: null, missing: "Employee leave", missingId: employeeLeaveId ]
                } else {
                    employeeLeave = new EmployeeLeave([leaveId: employeeLeaveId])
                }
            } else if (employeeLeave.status != "Pending"){
                return [ employeeLeave: employeeLeave, isStatusChanged: true ]
            }
        } else {
            employeeLeave = new EmployeeLeave()
        }

        Map employeeData = prepareEmployeeLeaveData(params, employeeId, patchEmployeeLeave)
        employeeLeave.properties = employeeData

        if(employeeLeave.hasChanged()){
            employeeLeave.save()
        }

        return [ employeeLeave: employeeLeave ]
    }

    Map prepareEmployeeLeaveData(JSONObject params, Long employeeId, Boolean patchEmployeeLeave) {
        Map employeeLeaveData = [:]
        if (patchEmployeeLeave){
            params.has("leaveType") ? employeeLeaveData.leaveType = params.get("leaveType").toString() : null
            params.has("description") ? employeeLeaveData.description = params.get("description").toString() : null
            params.has("startDate") ? employeeLeaveData.startDate = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("startDate").toString()) : null
            params.has("endDate") ? employeeLeaveData.endDate = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("endDate").toString()) : null
        } else {
            employeeLeaveData.leaveType = params.get("leaveType").toString()
            employeeLeaveData.description = params.get("description").toString()
            employeeLeaveData.startDate = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("startDate").toString())
            employeeLeaveData.endDate = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("endDate").toString())
            employeeLeaveData.employee = employeeService.getEmployee(employeeId)
        }
        return employeeLeaveData
    }
}
