package me.masum.hrm

import grails.gorm.transactions.Transactional
import groovy.sql.Sql
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat

@Transactional
class EmployeeService {
    DesignationService designationService
    DepartmentService departmentService
    def dataSource

    Employee getEmployee(Long employeeId) {
        return Employee.findByEmployeeId(employeeId)
    }

    Employee getEmployeeByEmail(String email) {
        return Employee.findByEmail(email)
    }

    Employee saveEmployee(JSONObject params){
        return validateAndSaveEmployee(params, null, false)
    }

    Employee updateEmployee(JSONObject params, Long employeeId){
        return validateAndSaveEmployee(params, employeeId, false)
    }

    Employee patchEmployee(JSONObject params, Long employeeId){
        return validateAndSaveEmployee(params, employeeId, true)
    }

    Boolean deleteEmployee(Long employeeId){
        Employee employee = Employee.findByEmployeeId(employeeId)
        if (employee){
            employee.delete()
            return true
        }
        return false
    }

    Map getEmployeeList(String query = null, Long currentPage = null,
                                   Integer limit) {

        currentPage.is(null) && (currentPage = 1)
        Long totalRow = 0

        List<Employee> employeeList = []
        def sql = Sql.newInstance(dataSource)
        def searchSQL = ""
        if(query){
            String designationIdList = designationService.getDesignationIdListBySearchTerm(query).toString()
            designationIdList = designationIdList.substring(1, designationIdList.length() - 1)

            String departmentIdList = departmentService.getDepartmentIdListBySearchTerm(query).toString()
            departmentIdList = departmentIdList.substring(1, departmentIdList.length() - 1)

            searchSQL = """
                WHERE LOWER(first_name) like LOWER('%${query}%') OR LOWER(middle_name) like LOWER('%${query}%') OR 
                LOWER(last_name) like LOWER('%${query}%') OR LOWER(email) like LOWER('%${query}%') """

            designationIdList.length() && (searchSQL += " OR designation_id in (${designationIdList})")
            departmentIdList.length() && (searchSQL += " OR department_id in (${departmentIdList})")
        }

        sql.rows("""
            WITH orderedTable AS (
                select 
                    ROW_NUMBER() over (
                        order by 
                            employee_id desc,
                            first_name,
                            last_name
                    ) as rowNum,
                    first_name as firstName,
                    middle_name as middleName,
                    last_name as lastName,
                    email,
                    date_of_birth as dateOfBirth,
                    joining_date as joiningDate,
                    department_id as departmentId,
                    designation_id as designationId,
                    employee_id as employeeId
                from employee """ + searchSQL + """
            )
            select *,
            (select MAX(rowNum) FROM orderedTable) AS 'totalRows'
            from orderedTable
            where rowNum >= ${1 + ((currentPage - 1) * limit)} and rowNum<= ${limit * currentPage};
        """
        ).each {
            Employee employee = new Employee(it)
            employee.department = Department.get(it.departmentId)
            employee.designation = Designation.get(it.designationId)
            employeeList.add(employee)
            totalRow = it.totalRows
        }
        def totalPages = totalRow / limit
        return [employeeList: employeeList, totalPages: totalPages.toLong() <= 0 ? 1 : totalPages.toInteger()]
    }

    private Employee validateAndSaveEmployee(JSONObject params, Long employeeId, Boolean patchEmployee) {
        Employee employee

        if (!employeeId.is(null)){
            employee = getEmployee(employeeId)
            if (employee == null) {
                if(!patchEmployee) {
                    employee = new Employee([employeeId: employeeId])
                } else {
                    return null
                }
            }
        } else {
            employee = new Employee()
        }

        Map employeeData = prepareEmployeeData(params, patchEmployee)
        employee.properties = employeeData

        if(employee.hasChanged()){
            employee.save()
        }

        return employee
    }

    Map prepareEmployeeData(JSONObject params, Boolean patchEmployee) {
        Map employeeData = [:]
        if (patchEmployee){
            params.has("firstName") ? employeeData.firstName = params.get("firstName").toString() : null
            params.has("middleName") ? employeeData.middleName = params.get("middleName").toString() : null
            params.has("lastName") ? employeeData.lastName = params.get("lastName").toString() : null
            params.has("email") ? employeeData.email = params.get("email").toString() : null
            params.has("dateOfBirth") ? employeeData.dateOfBirth = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(params.get("dateOfBirth").toString()) : null
            params.has("joiningDate") ? employeeData.joiningDate = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(params.get("joiningDate").toString()) : null
            params.has("department") ? employeeData.department = departmentService.getDepartment(params
                    .get("department").toString()) : null
            params.has("designation") ? employeeData.designation = designationService.getDesignation(params
                    .get("designation").toString()) : null
        } else {
            employeeData.firstName = params.get("firstName").toString()
            employeeData.middleName = params.get("middleName").toString()
            employeeData.lastName = params.get("lastName").toString()
            employeeData.email = params.get("email").toString()
            employeeData.dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("dateOfBirth").toString())
            employeeData.joiningDate = new SimpleDateFormat("yyyy-MM-dd").parse(params.get("joiningDate").toString())
            employeeData.department = departmentService.getDepartment(params.get("department").toString())
            employeeData.designation = designationService.getDesignation(params.get("designation").toString())
        }
        return employeeData
    }
}
