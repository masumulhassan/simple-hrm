package me.masum.hrm

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat

@Transactional
class DesignationService {


    def getDesignation(String designation){
        Designation designationRow = Designation.findByNameIlike(designation)
        if(designationRow){
            return designationRow
        }
        return saveDesignation(designation)
    }

    def getDesignationIdListBySearchTerm(String searchTerm){
        return Designation.findAllByNameIlike("%${searchTerm}%").getAt("id").flatten()
    }

    def saveDesignation(String designation){
        Designation designationRow = new Designation()
        designationRow.name = designation
        designationRow.save()
        return designationRow
    }
}
