package me.masum.hrm

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import me.masum.hrm.api.BasicAuthException

@Transactional
class UserService {
    BasicAuthTokenService basicAuthTokenService

    User get(Long id){
        return User.get(id)
    }

    User login(String email, String password){
        User user = User.findByEmailAndPassword(email, password.encodeAsSHA256().toString())
        if(user){
            if(!user.basicAuthToken.is(null)){
                user.basicAuthToken.accessToken = UUID.randomUUID().toString()
                user.basicAuthToken.refreshToken = UUID.randomUUID().toString()
                user.basicAuthToken.save()
            } else {
                BasicAuthToken basicAuthToken = basicAuthTokenService.generateToken()
                user.basicAuthToken = basicAuthToken
            }
            println user as JSON
            user.save()
            return user
        }
        return null
    }

    List<User> list(Map args){
        List<User> userList = User.list(args)
        return userList
    }

    Boolean delete(String email){
        User user = User.findByEmail(email).delete()
        if (user) {
            user.delete()
            return true
        }
        return false
    }

    User save(String name, String email, String password){
        User user = new User()
        user.name = name
        user.email = email
        user.password = password.encodeAsSHA256().toString()
        user.save()

        return user
    }

}
