package me.masum.hrm

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat

@Transactional
class DepartmentService {

    def getDepartment(String department){
        Department departmentObj = Department.findByNameIlike(department)
        if(departmentObj){
            return departmentObj
        }
        return saveDepartment(department)
    }

    def getDepartmentIdListBySearchTerm(String searchTerm){
        return Department.findAllByNameIlike("%${searchTerm}%").getAt("id").flatten()
    }

    def saveDepartment(String department){
        Department newDepartment = new Department()
        newDepartment.name = department
        newDepartment.save()
        return newDepartment
    }
}
