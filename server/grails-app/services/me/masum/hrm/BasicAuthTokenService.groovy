package me.masum.hrm


import grails.gorm.transactions.Transactional
import me.masum.hrm.api.BasicAuthException

@Transactional
class BasicAuthTokenService {

    BasicAuthToken get(String token){
        return BasicAuthToken.findByAccessToken(token)
    }

    BasicAuthToken getByRefreshToken(String token){
        return BasicAuthToken.findByRefreshToken(token)
    }

    void validate(String token){
        if (token) {
            token = token.replace("Bearer ", "")
            BasicAuthToken basicAuthToken = BasicAuthToken.findByAccessToken(token)
            if (basicAuthToken) {
                if(!(((new Date().time) - basicAuthToken.updated.time) <= basicAuthToken.duration * 1000)){
                    throw new BasicAuthException("Token expired", 401)
                }
            } else {
                throw new BasicAuthException("Invalid token", 401)
            }
        } else {
            throw new BasicAuthException("Invalid token", 401)
        }
    }

    BasicAuthToken generateToken(){
        BasicAuthToken basicAuthToken = new BasicAuthToken()
        basicAuthToken.accessToken = UUID.randomUUID().toString()
        basicAuthToken.refreshToken = UUID.randomUUID().toString()
        basicAuthToken.save()
        return basicAuthToken
    }

    BasicAuthToken generateToken(String refreshToken, String grantType) {
        if (grantType == "refresh_token") {
            BasicAuthToken basicAuthToken = getByRefreshToken(refreshToken)
            if (basicAuthToken) {
                basicAuthToken.accessToken = UUID.randomUUID().toString()
                basicAuthToken.refreshToken = UUID.randomUUID().toString()
                basicAuthToken.save()
                return basicAuthToken
            }
            return null
        }
        return null
    }
}
