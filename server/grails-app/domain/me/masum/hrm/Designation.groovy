package me.masum.hrm

class Designation {
    String name

    Collection<Employee> employees

    static hasMany = [employees: Employee]

    static constraints = {
        name blank: false
    }
}
