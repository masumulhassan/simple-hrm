package me.masum.hrm

class BasicAuthToken {
    String accessToken
    String refreshToken
    Integer duration = 3600         //in seconds

    Date created
    Date updated

    static constraints = {
        accessToken nullable: false, blank: false
        refreshToken nullable: false, blank: false
    }

    def beforeValidate() {
        if (!this.created) {
            this.created = new Date()
        }
        if (!this.updated) {
            this.updated = new Date()
        }
    }

    def beforeUpdate() {
        this.updated = new Date()
    }
}
