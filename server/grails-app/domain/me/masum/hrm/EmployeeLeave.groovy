package me.masum.hrm

import org.apache.commons.lang3.time.DateUtils

import java.text.SimpleDateFormat

class EmployeeLeave {
    Long        leaveId
    String      leaveType
    String      description
    Date        startDate
    Date        endDate
    Employee    employee
    String      status = "Pending"

    static belongsTo = [Employee]

    static constraints = {
        leaveId         nullable: true, blank: true, unique: true
        leaveType       inList: ["Sick leave", "Casual leave", "Maternity leave", "Paternity leave"], nullable: false, blank: false
        description     nullable: true, blank: true
        startDate       nullable: false, validator: {value, object ->
            if (object.startDate.compareTo(object.employee.joiningDate) < 0) return ['', "Start date can't be previous date than joining date. (joining date:" + (new SimpleDateFormat('yyyy-MM-dd')).format(object.employee.joiningDate) + ")"]
        }
        endDate         nullable: false, validator: {value, object ->
            if (object.startDate.compareTo(object.endDate) > 0) return ['',"Start date can't be higher than end date"]
        }
        employee        nullable: false
        status          inList: ["Approved", "Rejected", "Pending"], nullable: false, blank: false
    }

    def beforeInsert(){
        if (this.leaveId.is(null)) {
            while(true) {
                EmployeeLeave employeeLeave = EmployeeLeave.last()
                Long id = (!employeeLeave.is(null) ? employeeLeave.getId() : 0 ) + 1 + 500000
                if (EmployeeLeave.findByLeaveId(id).is(null)) {
                    this.leaveId = id
                    break
                }
            }
        }
    }
}
