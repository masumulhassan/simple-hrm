package me.masum.hrm

import org.apache.commons.lang3.time.DateUtils

class Employee {
    Long            employeeId
    String          firstName
    String          middleName
    String          lastName
    String          email
    Date            dateOfBirth
    Date            joiningDate
    Designation     designation
    Department      department

    Collection<EmployeeLeave> leaves

    static hasMany      = [leaves: EmployeeLeave]
    static belongsTo    = [Designation, Department]

    static constraints = {
        employeeId  nullable: true, blank: true, unique: true
        firstName   blank: false, nullable: false
        middleName  blank: true, nullable: true
        lastName    blank: true, nullable: true
        email       email: true, blank: false, nullable: false, unique: true
        dateOfBirth nullable: false, validator: {value, object ->
            if ((new Date()).compareTo(DateUtils.addYears(value, +18)) < 0) return ['',"Looks like your employee is too young. (less than 18 years old)"]
        }
        joiningDate nullable: false, validator: {value, object ->
            if ((value).compareTo(DateUtils.addYears(object.dateOfBirth, +18)) < 0) return ['',"Looks like your employee is too young for that joining date. (less than 18 years old, according to joining date)"]
        }
        designation nullable: false
        department  nullable: false
    }

    def beforeInsert(){
        if (this.employeeId.is(null)) {
            while(true) {
                Employee employee = Employee.last()
                Long employeeId = (!employee.is(null) ? employee.getId() : 0 ) + 1 + 100000
                if (Employee.findByEmployeeId(employeeId).is(null)) {
                    this.employeeId = employeeId
                    break
                }
            }
        }
    }

    String getFullName() {
        return this.firstName + ( this.middleName ? " " + this.middleName : "") + ( this.lastName ? " " + this.lastName : "")
    }
}
