package me.masum.hrm

class User {
    String name
    String email
    String password

    BasicAuthToken basicAuthToken

    static constraints = {
        name nullable: false, blank: false
        email email: true, nullable: false, blank: false, unique: true
        password nullable: false, blank: false
        basicAuthToken nullable: true, blank: true
    }
}
