package me.masum.hrm

import groovy.json.JsonSlurper
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

import java.text.SimpleDateFormat

class BootStrap {
    DepartmentService departmentService
    DesignationService designationService

    def init = { servletContext ->
        def jsonObject = [
                ["firstName": "Augusto", "middleName": "Penrice", "lastName": "Petche", "email": "apetche0@reference.com", "dateOfBirth": "27/07/1996", "joiningDate": "02/06/2020", "department": "Engineering", "designation": "Recruiter"],
                ["firstName": "Ulrike", "middleName": "Seid", "lastName": "Youle", "email": "uyoule1@columbia.edu", "dateOfBirth": "18/09/1995", "joiningDate": "01/04/2020", "department": "Business Development", "designation": "VP Product Management"],
                ["firstName": "Ciro", "middleName": "Deacock", "lastName": "Grayland", "email": "cgrayland2@vimeo.com", "dateOfBirth": "21/06/1993", "joiningDate": "01/08/2020", "department": "Accounting", "designation": "Staff Accountant IV"],
                ["firstName": "Geralda", "middleName": "Chapiro", "lastName": "Bagot", "email": "gbagot3@oracle.com", "dateOfBirth": "24/04/1998", "joiningDate": "06/02/2020", "department": "Business Development", "designation": "Quality Control Specialist"],
                ["firstName": "Marybelle", "middleName": "Capps", "lastName": "Menis", "email": "mmenis4@people.com.cn", "dateOfBirth": "01/01/1991", "joiningDate": "11/06/2020", "department": "Legal", "designation": "Junior Executive"],
                ["firstName": "Theresita", "middleName": "Bakeup", "lastName": "Potteril", "email": "tpotteril5@booking.com", "dateOfBirth": "31/05/1999", "joiningDate": "30/04/2020", "department": "Accounting", "designation": "Editor"],
                ["firstName": "Davis", "middleName": "Merrell", "lastName": "Kobierra", "email": "dkobierra6@digg.com", "dateOfBirth": "09/02/2000", "joiningDate": "13/01/2020", "department": "Services", "designation": "Research Associate"],
                ["firstName": "Jobina", "middleName": "Mordaunt", "lastName": "Larkin", "email": "jlarkin7@google.com.hk", "dateOfBirth": "11/02/1992", "joiningDate": "26/12/2019", "department": "Legal", "designation": "Recruiter"],
                ["firstName": "Idette", "middleName": "Dullingham", "lastName": "Higgoe", "email": "ihiggoe8@t-online.de", "dateOfBirth": "07/12/1998", "joiningDate": "18/10/2020", "department": "Services", "designation": "Nurse Practicioner"],
                ["firstName": "Pearle", "middleName": "Liquorish", "lastName": "Auger", "email": "pauger9@cmu.edu", "dateOfBirth": "11/06/1992", "joiningDate": "08/04/2020", "department": "Support", "designation": "Sales Representative"],
                ["firstName": "Ruperto", "middleName": "Chidlow", "lastName": "Eschelle", "email": "reschellea@discovery.com", "dateOfBirth": "23/05/1991", "joiningDate": "08/07/2020", "department": "Human Resources", "designation": "Pharmacist"],
                ["firstName": "Juliann", "middleName": "Sesons", "lastName": "Hughill", "email": "jhughillb@desdev.cn", "dateOfBirth": "21/02/1998", "joiningDate": "30/10/2019", "department": "Product Management", "designation": "Data Coordiator"],
                ["firstName": "Mikel", "middleName": "Forryan", "lastName": "Swepson", "email": "mswepsonc@dropbox.com", "dateOfBirth": "10/07/1998", "joiningDate": "30/03/2020", "department": "Human Resources", "designation": "Assistant Professor"],
                ["firstName": "Casi", "middleName": "Muffen", "lastName": "Fierman", "email": "cfiermand@nytimes.com", "dateOfBirth": "01/09/2000", "joiningDate": "19/01/2020", "department": "Business Development", "designation": "Administrative Assistant IV"],
                ["firstName": "Arvin", "middleName": "Coatsworth", "lastName": "Von Der Empten", "email": "avonderemptene@loc.gov", "dateOfBirth": "29/06/1995", "joiningDate": "16/03/2020", "department": "Legal", "designation": "Sales Associate"],
                ["firstName": "Bruce", "middleName": "Chamberlaine", "lastName": "Sheardown", "email": "bsheardownf@stanford.edu", "dateOfBirth": "04/04/2000", "joiningDate": "08/04/2020", "department": "Engineering", "designation": "Senior Financial Analyst"],
                ["firstName": "Keven", "middleName": "Ownsworth", "lastName": "Fawdrey", "email": "kfawdreyg@squarespace.com", "dateOfBirth": "03/12/1996", "joiningDate": "23/04/2020", "department": "Product Management", "designation": "Administrative Officer"],
                ["firstName": "Annetta", "middleName": "Fillingham", "lastName": "Gianneschi", "email": "agianneschih@godaddy.com", "dateOfBirth": "19/09/1998", "joiningDate": "20/06/2020", "department": "Services", "designation": "Automation Specialist II"],
                ["firstName": "Clevie", "middleName": "Ronayne", "lastName": "Seymour", "email": "cseymouri@dropbox.com", "dateOfBirth": "07/12/1993", "joiningDate": "06/12/2019", "department": "Business Development", "designation": "Assistant Manager"],
                ["firstName": "Jany", "middleName": "Challes", "lastName": "Normanvill", "email": "jnormanvillj@jimdo.com", "dateOfBirth": "23/09/1999", "joiningDate": "22/03/2020", "department": "Legal", "designation": "Analyst Programmer"],
                ["firstName": "Jillane", "middleName": "McGeachey", "lastName": "Karlolczak", "email": "jkarlolczakk@dot.gov", "dateOfBirth": "19/09/1997", "joiningDate": "21/01/2020", "department": "Human Resources", "designation": "Account Coordinator"],
                ["firstName": "Claribel", "middleName": "Sturgeon", "lastName": "Timson", "email": "ctimsonl@dailymail.co.uk", "dateOfBirth": "03/02/1996", "joiningDate": "18/07/2020", "department": "Research and Development", "designation": "Desktop Support Technician"],
                ["firstName": "Allistir", "middleName": "Blatchford", "lastName": "Steanson", "email": "asteansonm@yahoo.co.jp", "dateOfBirth": "20/07/2000", "joiningDate": "02/12/2019", "department": "Legal", "designation": "Social Worker"],
                ["firstName": "Dulciana", "middleName": "Leyninye", "lastName": "Shayes", "email": "dshayesn@goodreads.com", "dateOfBirth": "10/11/1990", "joiningDate": "15/08/2020", "department": "Services", "designation": "Environmental Tech"],
                ["firstName": "Lonny", "middleName": "Mawhinney", "lastName": "Overstall", "email": "loverstallo@squidoo.com", "dateOfBirth": "05/04/1992", "joiningDate": "24/11/2019", "department": "Training", "designation": "Nurse Practicioner"],
                ["firstName": "Laurel", "middleName": "Bohlmann", "lastName": "Cranna", "email": "lcrannap@economist.com", "dateOfBirth": "10/12/1998", "joiningDate": "02/12/2019", "department": "Legal", "designation": "Analyst Programmer"],
                ["firstName": "Celinka", "middleName": "Knowlton", "lastName": "Francklyn", "email": "cfrancklynq@facebook.com", "dateOfBirth": "23/05/1996", "joiningDate": "21/02/2020", "department": "Business Development", "designation": "Senior Financial Analyst"],
                ["firstName": "Richardo", "middleName": "Dommersen", "lastName": "Beddoe", "email": "rbeddoer@paginegialle.it", "dateOfBirth": "12/11/1999", "joiningDate": "08/07/2020", "department": "Product Management", "designation": "Technical Writer"],
                ["firstName": "Tory", "middleName": "Hambly", "lastName": "Mulbery", "email": "tmulberys@dyndns.org", "dateOfBirth": "12/08/1992", "joiningDate": "24/04/2020", "department": "Accounting", "designation": "Junior Executive"],
                ["firstName": "Ettie", "middleName": "Demkowicz", "lastName": "Gittus", "email": "egittust@engadget.com", "dateOfBirth": "22/09/1998", "joiningDate": "22/08/2020", "department": "Product Management", "designation": "Human Resources Assistant I"],
                ["firstName": "Adriaens", "middleName": "Symcox", "lastName": "Berndsen", "email": "aberndsenu@gnu.org", "dateOfBirth": "19/11/1993", "joiningDate": "17/12/2019", "department": "Marketing", "designation": "Sales Associate"],
                ["firstName": "Tressa", "middleName": "Tweedell", "lastName": "Skeat", "email": "tskeatv@arizona.edu", "dateOfBirth": "22/12/1992", "joiningDate": "22/08/2020", "department": "Research and Development", "designation": "Project Manager"],
                ["firstName": "Barnett", "middleName": "Saltman", "lastName": "Wisniowski", "email": "bwisniowskiw@yandex.ru", "dateOfBirth": "29/11/1996", "joiningDate": "11/09/2020", "department": "Research and Development", "designation": "Electrical Engineer"],
                ["firstName": "Elisabeth", "middleName": "Yanson", "lastName": "Trump", "email": "etrumpx@feedburner.com", "dateOfBirth": "01/09/1999", "joiningDate": "09/10/2020", "department": "Human Resources", "designation": "Desktop Support Technician"],
                ["firstName": "Muhammad", "middleName": "Berzons", "lastName": "Moxon", "email": "mmoxony@ycombinator.com", "dateOfBirth": "22/10/1991", "joiningDate": "08/03/2020", "department": "Accounting", "designation": "Help Desk Operator"],
                ["firstName": "Cedric", "middleName": "Hopfer", "lastName": "Mincini", "email": "cminciniz@economist.com", "dateOfBirth": "28/08/1998", "joiningDate": "09/07/2020", "department": "Legal", "designation": "Librarian"],
                ["firstName": "Nat", "middleName": "Sheepy", "lastName": "Krystek", "email": "nkrystek10@ycombinator.com", "dateOfBirth": "06/04/1995", "joiningDate": "10/01/2020", "department": "Accounting", "designation": "Account Coordinator"],
                ["firstName": "Lucais", "middleName": "Dragoe", "lastName": "Ipgrave", "email": "lipgrave11@51.la", "dateOfBirth": "06/05/2000", "joiningDate": "20/05/2020", "department": "Marketing", "designation": "Safety Technician II"],
                ["firstName": "Corilla", "middleName": "Pietz", "lastName": "Padfield", "email": "cpadfield12@icio.us", "dateOfBirth": "27/05/1997", "joiningDate": "16/03/2020", "department": "Accounting", "designation": "Legal Assistant"],
                ["firstName": "Dougie", "middleName": "Dunniom", "lastName": "Assaf", "email": "dassaf13@odnoklassniki.ru", "dateOfBirth": "06/10/1997", "joiningDate": "22/07/2020", "department": "Marketing", "designation": "Legal Assistant"],
                ["firstName": "Selena", "middleName": "Hrynczyk", "lastName": "Cocklie", "email": "scocklie14@constantcontact.com", "dateOfBirth": "24/02/1999", "joiningDate": "22/08/2020", "department": "Training", "designation": "Staff Accountant III"],
                ["firstName": "Ramsey", "middleName": "Guest", "lastName": "Knapp", "email": "rknapp15@addthis.com", "dateOfBirth": "19/11/1997", "joiningDate": "08/03/2020", "department": "Legal", "designation": "Electrical Engineer"],
                ["firstName": "Mady", "middleName": "Panketh", "lastName": "Kyteley", "email": "mkyteley16@bbc.co.uk", "dateOfBirth": "18/10/1995", "joiningDate": "03/10/2020", "department": "Accounting", "designation": "Senior Financial Analyst"],
                ["firstName": "Brade", "middleName": "Parsley", "lastName": "Balfe", "email": "bbalfe17@mapy.cz", "dateOfBirth": "09/04/1994", "joiningDate": "24/12/2019", "department": "Business Development", "designation": "Cost Accountant"],
                ["firstName": "Darlleen", "middleName": "Sieve", "lastName": "Sunner", "email": "dsunner18@stumbleupon.com", "dateOfBirth": "03/01/1998", "joiningDate": "11/07/2020", "department": "Accounting", "designation": "VP Accounting"],
                ["firstName": "Noel", "middleName": "Macias", "lastName": "Connick", "email": "nconnick19@lulu.com", "dateOfBirth": "10/05/1997", "joiningDate": "14/05/2020", "department": "Accounting", "designation": "Sales Representative"],
                ["firstName": "Jerrome", "middleName": "Colcutt", "lastName": "Wicken", "email": "jwicken1a@acquirethisname.com", "dateOfBirth": "18/08/1999", "joiningDate": "16/08/2020", "department": "Human Resources", "designation": "Recruiter"],
                ["firstName": "Rosie", "middleName": "Dowey", "lastName": "Arlow", "email": "rarlow1b@cisco.com", "dateOfBirth": "23/08/1994", "joiningDate": "23/07/2020", "department": "Legal", "designation": "Accounting Assistant III"],
                ["firstName": "Talbert", "middleName": "Abramov", "lastName": "Suermeiers", "email": "tsuermeiers1c@icio.us", "dateOfBirth": "05/12/1994", "joiningDate": "23/07/2020", "department": "Human Resources", "designation": "Administrative Assistant I"],
                ["firstName": "Amelina", "middleName": "Rudram", "lastName": "Brandoni", "email": "abrandoni1d@bloglovin.com", "dateOfBirth": "04/02/1999", "joiningDate": "29/11/2019", "department": "Sales", "designation": "Analyst Programmer"],
                ["firstName": "Chadd", "middleName": "Clarage", "lastName": "Johananoff", "email": "cjohananoff1e@cloudflare.com", "dateOfBirth": "05/08/2000", "joiningDate": "28/07/2020", "department": "Legal", "designation": "Food Chemist"],
                ["firstName": "Carlie", "middleName": "McGoldrick", "lastName": "Westman", "email": "cwestman1f@csmonitor.com", "dateOfBirth": "28/08/1998", "joiningDate": "06/06/2020", "department": "Marketing", "designation": "Programmer III"],
                ["firstName": "Robbie", "middleName": "Wedgbrow", "lastName": "Penketh", "email": "rpenketh1g@meetup.com", "dateOfBirth": "10/06/1997", "joiningDate": "14/07/2020", "department": "Services", "designation": "Information Systems Manager"],
                ["firstName": "Quintin", "middleName": "Gomez", "lastName": "McClean", "email": "qmcclean1h@ca.gov", "dateOfBirth": "20/06/1996", "joiningDate": "23/05/2020", "department": "Marketing", "designation": "Research Associate"],
                ["firstName": "Celle", "middleName": "Minigo", "lastName": "Dorber", "email": "cdorber1i@prlog.org", "dateOfBirth": "15/02/1998", "joiningDate": "03/11/2019", "department": "Research and Development", "designation": "Accountant II"],
                ["firstName": "Kristen", "middleName": "Simunek", "lastName": "Lindores", "email": "klindores1j@wikipedia.org", "dateOfBirth": "29/04/2000", "joiningDate": "22/07/2020", "department": "Engineering", "designation": "Administrative Officer"],
                ["firstName": "Winfred", "middleName": "Grills", "lastName": "Gipps", "email": "wgipps1k@aol.com", "dateOfBirth": "29/11/1998", "joiningDate": "06/10/2020", "department": "Services", "designation": "Staff Accountant I"],
                ["firstName": "Caresa", "middleName": "Whetnall", "lastName": "Polglaze", "email": "cpolglaze1l@flickr.com", "dateOfBirth": "23/06/1995", "joiningDate": "24/12/2019", "department": "Legal", "designation": "Civil Engineer"],
                ["firstName": "Ursa", "middleName": "Donohue", "lastName": "Dornin", "email": "udornin1m@booking.com", "dateOfBirth": "21/10/1994", "joiningDate": "27/11/2019", "department": "Human Resources", "designation": "Associate Professor"],
                ["firstName": "Jasen", "middleName": "Cloute", "lastName": "Hollibone", "email": "jhollibone1n@cpanel.net", "dateOfBirth": "24/08/1991", "joiningDate": "20/03/2020", "department": "Research and Development", "designation": "Mechanical Systems Engineer"],
                ["firstName": "Jock", "middleName": "Woolerton", "lastName": "Castello", "email": "jcastello1o@nationalgeographic.com", "dateOfBirth": "16/03/2000", "joiningDate": "07/12/2019", "department": "Marketing", "designation": "Programmer Analyst II"],
                ["firstName": "Claudetta", "middleName": "McDuffie", "lastName": "Rorke", "email": "crorke1p@amazonaws.com", "dateOfBirth": "06/02/1991", "joiningDate": "19/11/2019", "department": "Marketing", "designation": "Registered Nurse"],
                ["firstName": "Stephannie", "middleName": "Kynsey", "lastName": "Hansell", "email": "shansell1q@angelfire.com", "dateOfBirth": "27/12/1994", "joiningDate": "05/03/2020", "department": "Sales", "designation": "Information Systems Manager"],
                ["firstName": "Chester", "middleName": "Glacken", "lastName": "Jonson", "email": "cjonson1r@ucoz.ru", "dateOfBirth": "27/08/2000", "joiningDate": "12/03/2020", "department": "Sales", "designation": "Nurse Practicioner"],
                ["firstName": "Sally", "middleName": "Cody", "lastName": "Ferries", "email": "sferries1s@reuters.com", "dateOfBirth": "19/11/1992", "joiningDate": "03/07/2020", "department": "Training", "designation": "Quality Control Specialist"],
                ["firstName": "Emilie", "middleName": "Kingescot", "lastName": "Coleman", "email": "ecoleman1t@booking.com", "dateOfBirth": "10/05/1998", "joiningDate": "13/03/2020", "department": "Engineering", "designation": "Operator"],
                ["firstName": "Liuka", "middleName": "Scutchings", "lastName": "Kunrad", "email": "lkunrad1u@gravatar.com", "dateOfBirth": "14/08/1993", "joiningDate": "17/06/2020", "department": "Training", "designation": "Software Test Engineer IV"],
                ["firstName": "Devonne", "middleName": "Doucette", "lastName": "Gisbey", "email": "dgisbey1v@ihg.com", "dateOfBirth": "15/08/1999", "joiningDate": "13/04/2020", "department": "Services", "designation": "Analog Circuit Design manager"],
                ["firstName": "Nikos", "middleName": "Zappel", "lastName": "Strowan", "email": "nstrowan1w@telegraph.co.uk", "dateOfBirth": "14/11/1993", "joiningDate": "31/08/2020", "department": "Research and Development", "designation": "Project Manager"],
                ["firstName": "Kimble", "middleName": "Waszczykowski", "lastName": "Klishin", "email": "kklishin1x@com.com", "dateOfBirth": "13/01/1993", "joiningDate": "05/12/2019", "department": "Services", "designation": "Environmental Specialist"],
                ["firstName": "Coriss", "middleName": "Cicci", "lastName": "Busain", "email": "cbusain1y@apache.org", "dateOfBirth": "01/07/1999", "joiningDate": "09/02/2020", "department": "Training", "designation": "Tax Accountant"],
                ["firstName": "Page", "middleName": "Harmston", "lastName": "Devall", "email": "pdevall1z@virginia.edu", "dateOfBirth": "11/02/1991", "joiningDate": "15/09/2020", "department": "Product Management", "designation": "Administrative Assistant IV"],
                ["firstName": "Yettie", "middleName": "Plumptre", "lastName": "Tace", "email": "ytace20@cdbaby.com", "dateOfBirth": "25/08/1993", "joiningDate": "01/03/2020", "department": "Marketing", "designation": "Assistant Professor"],
                ["firstName": "Pernell", "middleName": "Dicken", "lastName": "Trainor", "email": "ptrainor21@businessweek.com", "dateOfBirth": "15/06/2000", "joiningDate": "26/11/2019", "department": "Marketing", "designation": "Health Coach IV"],
                ["firstName": "Hatty", "middleName": "Adger", "lastName": "Serchwell", "email": "hserchwell22@free.fr", "dateOfBirth": "16/01/1996", "joiningDate": "13/02/2020", "department": "Research and Development", "designation": "Staff Scientist"],
                ["firstName": "Lynea", "middleName": "Callaghan", "lastName": "Crab", "email": "lcrab23@etsy.com", "dateOfBirth": "23/05/1996", "joiningDate": "18/10/2020", "department": "Business Development", "designation": "Engineer III"],
                ["firstName": "Elissa", "middleName": "McGarel", "lastName": "Ternent", "email": "eternent24@biglobe.ne.jp", "dateOfBirth": "25/05/1993", "joiningDate": "21/04/2020", "department": "Research and Development", "designation": "Sales Representative"],
                ["firstName": "Roch", "middleName": "Belin", "lastName": "Hellwig", "email": "rhellwig25@army.mil", "dateOfBirth": "02/11/1994", "joiningDate": "16/09/2020", "department": "Product Management", "designation": "Business Systems Development Analyst"],
                ["firstName": "Tanner", "middleName": "McClurg", "lastName": "De la croix", "email": "tdelacroix26@dmoz.org", "dateOfBirth": "07/11/1990", "joiningDate": "14/04/2020", "department": "Research and Development", "designation": "Office Assistant I"],
                ["firstName": "Alard", "middleName": "Beneix", "lastName": "Fitzgerald", "email": "afitzgerald27@surveymonkey.com", "dateOfBirth": "10/08/1995", "joiningDate": "13/11/2019", "department": "Sales", "designation": "Safety Technician I"],
                ["firstName": "Annice", "middleName": "Collisson", "lastName": "Trudgeon", "email": "atrudgeon28@fotki.com", "dateOfBirth": "07/05/1997", "joiningDate": "15/07/2020", "department": "Marketing", "designation": "Paralegal"],
                ["firstName": "Winfield", "middleName": "Copson", "lastName": "Baselli", "email": "wbaselli29@spiegel.de", "dateOfBirth": "21/12/1998", "joiningDate": "29/02/2020", "department": "Human Resources", "designation": "Marketing Manager"],
                ["firstName": "Ivor", "middleName": "Wilgar", "lastName": "Pearson", "email": "ipearson2a@bloglovin.com", "dateOfBirth": "24/01/1998", "joiningDate": "12/07/2020", "department": "Training", "designation": "Senior Developer"],
                ["firstName": "Dewain", "middleName": "Ceschelli", "lastName": "Calderbank", "email": "dcalderbank2b@usgs.gov", "dateOfBirth": "03/06/1997", "joiningDate": "30/05/2020", "department": "Accounting", "designation": "Product Engineer"],
                ["firstName": "Louie", "middleName": "Fullerton", "lastName": "Haresnaip", "email": "lharesnaip2c@dailymail.co.uk", "dateOfBirth": "24/03/1993", "joiningDate": "12/03/2020", "department": "Sales", "designation": "Project Manager"],
                ["firstName": "Jacquelin", "middleName": "Greedier", "lastName": "Gibling", "email": "jgibling2d@hao123.com", "dateOfBirth": "12/05/2000", "joiningDate": "22/06/2020", "department": "Research and Development", "designation": "Research Nurse"],
                ["firstName": "Rochelle", "middleName": "Divers", "lastName": "Reubbens", "email": "rreubbens2e@washington.edu", "dateOfBirth": "05/01/1991", "joiningDate": "21/06/2020", "department": "Training", "designation": "Administrative Assistant I"],
                ["firstName": "Velma", "middleName": "Norton", "lastName": "Hawksby", "email": "vhawksby2f@technorati.com", "dateOfBirth": "23/02/1993", "joiningDate": "19/05/2020", "department": "Services", "designation": "Internal Auditor"],
                ["firstName": "Janith", "middleName": "Rolinson", "lastName": "Hindenberger", "email": "jhindenberger2g@hp.com", "dateOfBirth": "30/04/1997", "joiningDate": "11/09/2020", "department": "Services", "designation": "Research Associate"],
                ["firstName": "Theresina", "middleName": "Wreford", "lastName": "Kubek", "email": "tkubek2h@ow.ly", "dateOfBirth": "12/10/1997", "joiningDate": "29/12/2019", "department": "Services", "designation": "VP Marketing"],
                ["firstName": "Hannis", "middleName": "Simkiss", "lastName": "de Guerre", "email": "hdeguerre2i@merriam-webster.com", "dateOfBirth": "13/02/2000", "joiningDate": "07/06/2020", "department": "Business Development", "designation": "Systems Administrator I"],
                ["firstName": "Kathie", "middleName": "Cridlan", "lastName": "Feye", "email": "kfeye2j@icq.com", "dateOfBirth": "25/04/1995", "joiningDate": "09/12/2019", "department": "Sales", "designation": "Financial Advisor"],
                ["firstName": "Schuyler", "middleName": "Knotton", "lastName": "Slegg", "email": "sslegg2k@ft.com", "dateOfBirth": "10/10/1999", "joiningDate": "03/06/2020", "department": "Services", "designation": "Help Desk Operator"],
                ["firstName": "Walsh", "middleName": "Grundon", "lastName": "Bassick", "email": "wbassick2l@jiathis.com", "dateOfBirth": "08/02/1999", "joiningDate": "31/05/2020", "department": "Training", "designation": "GIS Technical Architect"],
                ["firstName": "Jessica", "middleName": "Midford", "lastName": "Righy", "email": "jrighy2m@netvibes.com", "dateOfBirth": "10/03/1999", "joiningDate": "20/06/2020", "department": "Services", "designation": "Environmental Tech"],
                ["firstName": "Gill", "middleName": "Kippen", "lastName": "Lippett", "email": "glippett2n@example.com", "dateOfBirth": "17/11/1995", "joiningDate": "21/08/2020", "department": "Sales", "designation": "Food Chemist"],
                ["firstName": "Kele", "middleName": "Sambells", "lastName": "Sandal", "email": "ksandal2o@geocities.com", "dateOfBirth": "28/06/2000", "joiningDate": "29/11/2019", "department": "Accounting", "designation": "Professor"],
                ["firstName": "Scott", "middleName": "Atheis", "lastName": "Klicher", "email": "sklicher2p@latimes.com", "dateOfBirth": "24/08/1991", "joiningDate": "08/04/2020", "department": "Product Management", "designation": "Administrative Assistant I"],
                ["firstName": "Jillayne", "middleName": "Leathwood", "lastName": "Traher", "email": "jtraher2q@aol.com", "dateOfBirth": "18/02/1995", "joiningDate": "08/11/2019", "department": "Accounting", "designation": "Director of Sales"],
                ["firstName": "Eamon", "middleName": "Evershed", "lastName": "Stearnes", "email": "estearnes2r@skype.com", "dateOfBirth": "17/11/1996", "joiningDate": "24/05/2020", "department": "Research and Development", "designation": "Paralegal"]]

        jsonObject.each {
            (new Employee([
                    firstName: it.firstName,
                    middleName: it.middleName,
                    lastName: it.lastName,
                    email: it.email,
                    dateOfBirth: new SimpleDateFormat("dd/MM/yyyy").parse(it.dateOfBirth),
                    joiningDate: new SimpleDateFormat("dd/MM/yyyy").parse(it.joiningDate),
                    department: departmentService.getDepartment(it.department),
                    designation: designationService.getDesignation(it.designation)
            ])
            ).save()
        }

        (new User([name: "HR Admin", email: "hr@simple-hrm.com", password: "simple-hrm".encodeAsSHA256().toString()])).save()
    }
    def destroy = {
    }
}
