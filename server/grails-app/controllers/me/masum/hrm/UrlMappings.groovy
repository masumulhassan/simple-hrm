package me.masum.hrm

class UrlMappings {

    static mappings = {

        //Employee API url mappings

        get "/api/v1/search"(controller: 'employee', action: 'search')
        delete "/api/v1/employee/$id(.$format)?"(controller: 'employee', action: 'delete')
        get "/api/v1/employee(.$format)?"(controller: 'employee', action: 'index')
        get "/api/v1/employee/$id(.$format)?"(controller: 'employee', action: 'show')
        post "/api/v1/employee(.$format)?"(controller: 'employee', action: 'save')
        put "/api/v1/employee/$id(.$format)?"(controller: 'employee', action: 'update')
        patch "/api/v1/employee/$id(.$format)?"(controller: 'employee', action: 'patch')

        //Employee's leave API url mappings
        delete "/api/v1/employee/$id/leave/$id2(.$format)?"(controller: 'employeeLeave', action: 'delete')
        get "/api/v1/employee/$id/leave(.$format)?"(controller: 'employeeLeave', action: 'index')
        get "/api/v1/employee/$id/leave/$id2(.$format)?"(controller: 'employeeLeave', action: 'show')
        post "/api/v1/employee/$id/leave(.$format)?"(controller: 'employeeLeave', action: 'save')
        put "/api/v1/employee/$id/leave/$id2(.$format)?"(controller: 'employeeLeave', action: 'update')
        patch "/api/v1/employee/$id/leave/$id2(.$format)?"(controller: 'employeeLeave', action: 'patch')
        get "/api/v1/employee/$id/approve-leave/$id2"(controller: 'employeeLeave', action: 'approveLeave')
        get "/api/v1/employee/$id/reject-leave/$id2"(controller: 'employeeLeave', action: 'rejectLeave')

        //Login url
        post "/api/v1/login?"(controller: 'user', action: 'login')
        post "/api/v1/token?"(controller: 'user', action: 'token')

        "/"(view: '/notFound')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
