package me.masum.hrm

import org.grails.web.json.JSONObject

class EmployeeController {

    EmployeeService employeeService

    def index(){
        Integer limit = 12
        if(params?.employeeBefore && params?.employeeAfter){
            [isError: true]
        } else {
            Map result = employeeService.getEmployeeList(null, params.currentPage as Long, limit)
            List<Employee> employeeList = result.employeeList
            [
                employeeList  : employeeList,
                totalPages: result.totalPages
            ]
        }
    }

    def show(){
        Employee employee = employeeService.getEmployee(params.id as Long)
        [employee: employee, employeeId: params.id]
    }

    def save(){
        JSONObject params = request.getJSON() as JSONObject
        Employee employee = employeeService.saveEmployee(params)
        [employee: employee]
    }

    def update(){
        JSONObject jsonObject = request.getJSON() as JSONObject
        Employee employee = employeeService.updateEmployee(jsonObject, params.id as Long)
        [employee: employee]
    }

    def patch(){
        JSONObject jsonObject = request.getJSON() as JSONObject
        Employee employee = employeeService.patchEmployee(jsonObject, params.id as Long)
        [employee: employee, employeeId: params.id]
    }

    def delete(){
        Boolean isDeleted = employeeService.deleteEmployee(params.id as Long)
        [isDeleted: isDeleted, employeeId: params.id]
    }

    def search(){
        Integer limit = 12
        if(params?.employeeBefore && params?.employeeAfter){
            render(view: "index", model: [isError: true])
        } else {
            Map result = employeeService.getEmployeeList(params.q as String, params.currentPage as Long, limit)
            List<Employee> employeeList = result.employeeList
            [
                    employeeList  : employeeList,
                    totalPages: result.totalPages
            ]
        }
    }
}
