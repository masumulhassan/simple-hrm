package me.masum.hrm

import grails.converters.JSON
import me.masum.hrm.api.BasicAuthException


class BasicAuthInterceptor {
    BasicAuthTokenService basicAuthTokenService

    BasicAuthInterceptor(){
        match(controller: "employee")
        match(controller: "employeeLeave")
    }

    boolean before() {
        try{
            String basicAuthToken = request.getHeader("Authorization")
            basicAuthTokenService.validate(basicAuthToken)
        } catch (BasicAuthException basicAuthException) {
            modelAndView = null
            response.setStatus(basicAuthException.statusCode)
            render([status: "error", message: basicAuthException.message, code: basicAuthException.statusCode] as JSON)
            return false
        }
        return true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
