package me.masum.hrm


import org.grails.web.json.JSONObject

class UserController {
    BasicAuthTokenService basicAuthTokenService
    UserService userService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond userService.list(params)
    }

    def show(Long id) {
        respond userService.get(id)
    }

    def login() {
        JSONObject loginData = request.getJSON() as JSONObject
        User user = userService.login(loginData.email, loginData.password)
        [user: user]
    }

    def token() {
        JSONObject tokenData = request.getJSON() as JSONObject
        BasicAuthToken basicAuthToken = basicAuthTokenService.generateToken(tokenData.refresh_token, tokenData.grant_type)
        [basicAuthToken: basicAuthToken]
    }

//    def save() {
//        JSONObject loginData = request.getJSON() as JSONObject
//        if (user == null) {
//            render status: NOT_FOUND
//            return
//        }
//        if (user.hasErrors()) {
//            transactionStatus.setRollbackOnly()
//            respond user.errors
//            return
//        }
//
//        try {
//            userService.save(user)
//        } catch (ValidationException e) {
//            respond user.errors
//            return
//        }
//
//        respond user, [status: CREATED, view:"show"]
//    }
//
//    def update(r) {
//        if (user == null) {
//            render status: NOT_FOUND
//            return
//        }
//        if (user.hasErrors()) {
//            transactionStatus.setRollbackOnly()
//            respond user.errors
//            return
//        }
//
//        try {
//            userService.save(user)
//        } catch (ValidationException e) {
//            respond user.errors
//            return
//        }
//
//        respond user, [status: OK, view:"show"]
//    }
//
//    def delete() {
//        if (id == null || userService.delete(id) == null) {
//            render status: NOT_FOUND
//            return
//        }
//
//        render status: NO_CONTENT
//    }
}
