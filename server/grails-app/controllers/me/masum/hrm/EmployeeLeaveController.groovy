package me.masum.hrm


import org.grails.web.json.JSONObject

class EmployeeLeaveController {
    EmployeeLeaveService employeeLeaveService

    def index() {

    }

    def show(){
        employeeLeaveService.getEmployeeLeaveDetails(params.id.toLong(), params.id2.toLong())
    }

    def save(){
        JSONObject jsonObject = request.getJSON() as JSONObject
        employeeLeaveService.saveEmployeeLeave(jsonObject, params.id.toLong())
    }

    def update(){
        JSONObject jsonObject = request.getJSON() as JSONObject
        employeeLeaveService.updateEmployeeLeave(jsonObject, params.id.toLong(), params.id2.toLong())
    }

    def patch(){
        JSONObject jsonObject = request.getJSON() as JSONObject
        employeeLeaveService.patchEmployeeLeave(jsonObject, params.id.toLong(), params.id2.toLong())
    }

    def delete(){
        employeeLeaveService.deleteEmployeeLeave(params.id.toLong(), params.id2.toLong())
    }

    def approveLeave(){
        employeeLeaveService.approveLeave(params.id.toLong(), params.id2.toLong())
    }

    def rejectLeave(){
        employeeLeaveService.rejectLeave(params.id.toLong(), params.id2.toLong())
    }
}
