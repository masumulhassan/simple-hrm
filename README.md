# Simple HRM #

Simple HRM is a small rest api and reactjs based employee and employee leave management application

### Directory structure ###

* `client` 			-> Reactjs application
* `server`			-> Grails based rest api

### Running requirement ###

* mysql 8 or higher
* jdk 8 or heigher
* npm or yarn

### Setup step ###

* For client app: go to client folder and run `yarn install` and than `yarn start`
* For server app: 
	* go to server folder
	* change datasource configuration based on your setup in `/server/grails-app/conf/application.yml`
	* go to server folder's root and run `./gradlew bootRun` or `./grails run-app`
