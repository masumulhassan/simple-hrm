import AppUtil from "./AppUtil";
import APICaller from "./APICaller";
import {toast} from "react-toastify";

export default class AuthenticationService {

    addAuthorizationMetaData(responseData) {
        let accessToken = responseData.access_token;
        let refreshToken = responseData.refresh_token;
        let tokenType = responseData.token_type;
        if (accessToken && refreshToken) {
            localStorage.setItem("access_token", accessToken);
            localStorage.setItem("refresh_token", refreshToken);
            localStorage.setItem("token_type", tokenType);
            return true;
        }
        return false;
    }

    processLogin(responseData) {
        if (this.addAuthorizationMetaData(responseData)) {
            localStorage.setItem("isAuthorized", "true");
            return true;
        }
        return false;
    }

    isLoggedIn() {
        const isAuthorized = localStorage.getItem("isAuthorized");
        const accessToken = localStorage.getItem("access_token");
        const refreshToken = localStorage.getItem("refresh_token");
        if (isAuthorized !== null && accessToken !== null && refreshToken !== null) {
            return true;
        }
        return false;
    }

    renewAuthorization(trHttpCall) {
        let requestData = {
            refresh_token: localStorage.getItem("refresh_token"),
            grant_type: localStorage.getItem("grant_type")
        };
        new APICaller().POST("api/v1/token", requestData,
            (response) => {
                if (response && this.addAuthorizationMetaData(response)) {
                    AppUtil.redirectTo("/")
                } else {
                    localStorage.clear();
                    AppUtil.redirectTo("/login");
                }
            },
            (response) => {
                toast.error("Unable to process request when trying to renew session");
                AppUtil.redirectTo("/login");
            })
    }


    static instance() {
        return new AuthenticationService();
    }

}
