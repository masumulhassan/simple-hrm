import axios from "axios";
import AppUtil from "./AppUtil";
import AuthenticationService from "./AuthenticationService";
import 'react-toastify/dist/ReactToastify.css';

class APICaller {
    options = {};
    constructor() {
        this.options = {
            headers: {'Authorization': 'Bearer ' + localStorage.getItem("access_token")}
        };
    }

    prepareFullApiUrl(url) {
        let apiUrl = localStorage.getItem("api_url")
        return apiUrl + url
    }

    addJSONHeader() {
        this.options.headers["content-type"] = "application/json";
    }

    checkErrorResponse(error) {
        let response = error.response
        if(response.status === 401){
            if(response.data.message === "Invalid token"){
                AppUtil.redirectTo("/login");
            } else if(response.data.message === "Token expired") {
                AuthenticationService.instance().renewAuthorization()
            }
        }
    }

    async GET(url, successCallback, errorCallback) {
        try {
            const response = await axios.get(this.prepareFullApiUrl(url), this.options);
            if (successCallback) {
                successCallback(await response);
            }
        } catch (err) {
            this.checkErrorResponse(err)
            if (errorCallback) {
                errorCallback(await err.response);
            }
        }
    }

    async POST(url, params, successCallback, errorCallback) {
        this.addJSONHeader()
        try {
            const response = await axios.post(this.prepareFullApiUrl(url), params, this.options);
            if (successCallback) {
                successCallback(await response);
            }
        } catch (err) {
            this.checkErrorResponse(err)
            if (errorCallback) {
                errorCallback(await err.response);
            }
        }
    }

    async PUT(url, params, successCallback, errorCallback) {
        this.addJSONHeader()
        try {
            const response = await axios.put(this.prepareFullApiUrl(url), params, this.options);
            if (successCallback) {
                successCallback(await response);
            }
        } catch (err) {
            this.checkErrorResponse(err)
            if (errorCallback) {
                errorCallback(await err.response);
            }
        }
    }

    async PATCH(url, params, successCallback, errorCallback) {
        this.addJSONHeader()
        try {
            const response = await axios.patch(this.prepareFullApiUrl(url), params, this.options);
            if (successCallback) {
                successCallback(await response);
            }
        } catch (err) {
            this.checkErrorResponse(err)
            if (errorCallback) {
                errorCallback(await err.response);
            }
        }
    }

    async DELETE(url, successCallback, errorCallback) {
        try {
            const response = await axios.delete(this.prepareFullApiUrl(url), this.options);
            if (successCallback) {
                successCallback(await response);
            }
        } catch (err) {
            this.checkErrorResponse(err)
            if (errorCallback) {
                errorCallback(await err.response);
            }
        }
    }
}

export default APICaller;
