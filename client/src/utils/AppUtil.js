const AppUtil = {
    pretifyName(name) {
        return name.replace(/[-_.]/g, ' ')
            .replace(/ +/g, ' ')
            .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
            .split(' ')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1))
            .join(' ');
    },
    redirectTo(url) {
        window.location = url
    },
    clean(obj) {
        let propNames = Object.getOwnPropertyNames(obj);
        for (let i = 0; i < propNames.length; i++) {
            let propName = propNames[i];
            if (obj[propName] === null || obj[propName] === undefined) {
                obj[propName] = "";
            }
        }
        return obj
    }
}

export default AppUtil
