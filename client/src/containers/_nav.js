export default [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Navigation']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Employee',
    to: '/employees',
    icon: 'cil-people',
  }
]

