import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import APICaller from "../../../utils/APICaller";
import {toast, ToastContainer} from "react-toastify";
import AuthenticationService from "../../../utils/AuthenticationService";
import AppUtil from "../../../utils/AppUtil";

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    }
  }


  login = (event) =>{
    event.preventDefault();
    let loginData = {
      email: this.state.email,
      password: this.state.password
    }
    new APICaller().POST("api/v1/login", loginData, (response)=>{
        if(AuthenticationService.instance().processLogin(response.data)){
          AppUtil.redirectTo("/")
        }
    },(err)=>{
        toast.error(err.data.message)
    })
  }


  render() {
    return (
        <div className="c-app c-default-layout flex-row align-items-center">
          <ToastContainer />
          <CContainer>
            <CRow className="justify-content-center">
              <CCol md="8">
                <CCardGroup>
                  <CCard className="p-4">
                    <CCardBody>
                      <form onSubmit={this.login}>
                        <h1>Login</h1>
                        <p className="text-muted">Sign In to your account</p>
                        <CInputGroup className="mb-3">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-user"/>
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                              type="email"
                              placeholder="Email"
                              autoComplete="email"
                              value={this.state.email}
                              required
                              onChange={(event) => {
                                this.setState({email: event.target.value});
                              }}
                          />
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-lock-locked"/>
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                              type="password"
                              placeholder="Password"
                              autoComplete="current-password"
                              value={this.state.password}
                              required
                              onChange={(event) => {
                                this.setState({password: event.target.value});
                              }}
                          />
                        </CInputGroup>
                        <CRow>
                          <CCol xs="6">
                            <CButton type="submit" color="primary" className="px-4" >Login</CButton>
                          </CCol>
                        </CRow>
                      </form>
                    </CCardBody>
                  </CCard>
                </CCardGroup>
              </CCol>
            </CRow>
          </CContainer>
        </div>
    )
  }
}

export default Login
