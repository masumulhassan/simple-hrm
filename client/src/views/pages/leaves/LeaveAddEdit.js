import React from 'react'
import {
    CButton, CButtonGroup,
    CCard,
    CCardBody, CCardHeader,
    CCol,
    CContainer,
    CFormGroup, CFormText, CInput, CLabel,
    CRow, CSelect, CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import APICaller from "../../../utils/APICaller";
import {toast} from "react-toastify";
import AppUtil from "../../../utils/AppUtil";

class LeaveAddEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            leave: {
                leaveType: "Sick leave",
                description: "",
                startDate: "",
                endDate: ""
            }
        }
    }

    getLeaveDetails = () => {
        new APICaller().GET("api/v1/employee/" + this.props.match.params.id + "/leave/" + this.props.match.params.leaveId, (response) => {
            delete response.data.leaves;
            this.setState({
                leave: AppUtil.clean(response.data)
            })
        }, (response) => {
            this.setState({"leaveDetails": [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]})
        })
    }

    submitLeaveAddEditForm = (event) => {
        event.preventDefault()
        if(this.props.match.params.id !== undefined && this.props.match.params.leaveId !== undefined) {
            new APICaller().PATCH("api/v1/employee/" + this.props.match.params.id + "/leave/" + this.props.match.params.leaveId, this.state.leave, (response) => {
                toast.success("Leave information updated successfully")
                this.props.history.push("/employee/" + this.props.match.params.id)
            }, (response) => {
                response.data.errors.forEach((value => {
                    toast.error(value.message)
                }))
            })
        } else {
            new APICaller().POST("api/v1/employee/" + this.props.match.params.id + "/leave", this.state.leave, (response) => {
                toast.success("Leave information added successfully")
                this.props.history.push("/employee/" + this.props.match.params.id)
            }, (response) => {
                response.data.errors.forEach((value => {
                    toast.error(value.message)
                }))
            })
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined && this.props.match.params.leaveId !== undefined) {
            this.getLeaveDetails()
        }
    }

    render() {
        return (
            <CContainer fluid>
                <CRow>
                    <CCol xs="12" md="6">
                        <CCard>
                            <CCardHeader>
                                Employee Leave
                            </CCardHeader>
                            <CCardBody>
                                <form className="form-horizontal" onSubmit={this.submitLeaveAddEditForm}>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="leave-type-input">Leave type</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CSelect
                                                custom
                                                id="leave-type-input"
                                                name="leave-type-input"
                                                required
                                                value={this.state.leave.leaveType}
                                                onChange={(e)=>{
                                                    this.state.leave.leaveType = e.target.value;
                                                    this.setState({leave: this.state.leave})
                                                }}
                                            >
                                                <option value="Sick leave">Sick leave</option>
                                                <option value="Casual leave">Casual leave</option>
                                                <option value="Maternity leave">Maternity leave</option>
                                                <option value="Paternity leave">Paternity leave</option>
                                            </CSelect>
                                        </CCol>
                                    </CFormGroup>

                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="description-input">Description</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CTextarea
                                                id="description-input"
                                                name="description-input"
                                                placeholder="Enter leave description"
                                                value={this.state.leave.description}
                                                onChange={(e)=>{
                                                    this.state.leave.description = e.target.value;
                                                    this.setState({leave: this.state.leave})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="start-date">Start date</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                type="date"
                                                id="start-date"
                                                name="start-date"
                                                placeholder="Leave start date"
                                                required
                                                value={this.state.leave.startDate}
                                                onChange={(e)=>{
                                                    this.state.leave.startDate = e.target.value;
                                                    this.setState({leave: this.state.leave})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="end-date">End date</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                type="date"
                                                id="end-date"
                                                name="end-date"
                                                placeholder="date"
                                                required
                                                value={this.state.leave.endDate}
                                                onChange={(e)=>{
                                                    this.state.leave.endDate = e.target.value;
                                                    this.setState({leave: this.state.leave})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="12" align="center">
                                            <CButtonGroup>
                                                <CButton
                                                    type="submit"
                                                    color="info"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                >Submit</CButton>
                                                <CButton
                                                    color="danger"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                    onClick={()=>this.props.history.push("/employee/" + this.props.match.params.id)}
                                                >Cancel</CButton>
                                            </CButtonGroup>
                                        </CCol>
                                    </CFormGroup>
                                </form>
                            </CCardBody>
                        </CCard>
                    </CCol>
                </CRow>
            </CContainer>
        )
    }
}

export default LeaveAddEdit
