import React from 'react'
import {
    CBadge,
    CButton, CButtonGroup,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CCollapse,
    CDataTable, CModal, CModalBody, CModalFooter, CModalHeader,
    CPagination,
    CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import APICaller from "../../../utils/APICaller";
import AppUtil from "../../../utils/AppUtil";
import {toast} from "react-toastify";

class Employee extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employeeDetails: [],
            leaves: [],
            details: [],
            modal: false,
            leaveIdToDelete: null
        }
    }

    getEmployeeDetails = () => {
        new APICaller().GET("api/v1/employee/" + this.props.match.params.id, (response) => {
            let leaves = response.data.leaves;
            delete response.data.leaves;
            delete response.data.name;
            this.setState({
                leaves: leaves,
                employeeDetails: Object.entries(response.data)
            })
        }, (response) => {
            console.log(response)
            this.setState({"employeeDetails": [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]})
        })
    }

    approveLeave = (leaveId, index) => {
        new APICaller().GET("api/v1/employee/" + this.props.match.params.id + "/approve-leave/" + leaveId, (response) => {
            toast.success("Leave approved")
            this.getEmployeeDetails()
        }, (response) => {
            console.log(response)
        })
    }

    rejectLeave = (leaveId, index) => {
        new APICaller().GET("api/v1/employee/" + this.props.match.params.id + "/reject-leave/" + leaveId, (response) => {
            toast.success("Leave rejected")
            this.getEmployeeDetails()
        }, (response) => {
            console.log(response)
        })
    }

    deleteLeave = () => {
        if (this.state.leaveIdToDelete !== null) {
            new APICaller().DELETE("api/v1/employee/" + this.props.match.params.id + "/leave/" + this.state.leaveIdToDelete, (response) => {
                toast.success("Leave deleted successfully")
                this.getEmployeeDetails()
            }, (response) => {
                console.log(response)
            })
        }
        this.setState({modal: false, leaveIdToDelete: null})
    }

    toggle = (action) => {
        if(action === "open") {
            this.setState({modal: true})
        } else {
            this.setState({modal: false, leaveIdToDelete: null})
        }
    }

    toggleDetails = (index) => {
        const position = this.state.details.indexOf(index)
        let newDetails = this.state.details.slice()
        if (position !== -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...this.state.leaves, index]
        }
        this.setState({details: newDetails})
    }

    getBadge = (status) => {
        switch (status) {
            case 'Approved':
                return 'success'
            case 'Pending':
                return 'warning'
            case 'Rejected':
                return 'danger'
            default:
                return 'primary'
        }
    }

    componentDidMount() {
        this.getEmployeeDetails()
    }

    render() {
        return (
            <CRow>
                <CCol lg={4}>
                    <CCard>
                        <CCardHeader>
                            Employee details
                        </CCardHeader>
                        <CCardBody>
                            <table className="table table-striped table-hover">
                                <tbody>
                                {
                                    this.state.employeeDetails.map(([key, value], index) => (
                                            <tr key={index.toString()}>
                                                <td>{`${AppUtil.pretifyName(key)}:`}</td>
                                                <td><strong>{value}</strong></td>
                                            </tr>
                                        )
                                    )
                                }
                                </tbody>
                            </table>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol lg={8}>
                    <CCard>
                        <CCardHeader>
                            Leaves
                        </CCardHeader>
                        <CCardBody>
                            <div className="row my-2 mx-0">
                                <div className="col-xl-2 form-inline" align="right">
                                    <CButton
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => this.props.history.push(`/employee/` + this.props.match.params.id + "/leave-add")}
                                    >
                                        Add Leave
                                    </CButton>
                                </div>
                            </div>
                            <CDataTable
                                items={this.state.leaves}
                                fields={[
                                    {key: 'leaveType', _classes: 'font-weight-bold'},
                                    'startDate', 'endDate', {key: 'status', sorter: true} , {
                                        key: 'show_details',
                                        label: 'Actions',
                                        _style: { width: '1%' },
                                        sorter: false,
                                        filter: false
                                    }
                                ]}
                                hover
                                striped
                                itemsPerPage={10}
                                pagination
                                scopedSlots = {{
                                    'status':
                                        (item)=>(
                                            <td>
                                                <CBadge color={this.getBadge(item.status)}>
                                                    {item.status}
                                                </CBadge>
                                            </td>
                                        ),
                                    'show_details':
                                        (item, index)=>{
                                            return (
                                                <td className="py-2">
                                                    <CButtonGroup>
                                                    <CButton
                                                        size="sm"
                                                        variant="outline"
                                                        shape="square"
                                                        color="danger"
                                                        onClick={()=>{
                                                            this.state.leaveIdToDelete = item.leaveId;
                                                            this.toggle("open")}}>
                                                        Delete
                                                    </CButton>
                                                    <CButton
                                                        color="primary"
                                                        variant="outline"
                                                        shape="square"
                                                        size="sm"
                                                        onClick={()=>{this.toggleDetails(index)}}
                                                    >
                                                        {this.state.details.includes(index) ? 'Hide' : 'Show'}
                                                    </CButton>
                                                    </CButtonGroup>
                                                </td>
                                            )
                                        },
                                    'details':
                                        (item, index)=>{
                                            return (
                                                <CCollapse show={this.state.details.includes(index)}>
                                                    <CCardBody>
                                                        <h4>
                                                            Description:
                                                        </h4>
                                                        <p className="text-muted">{item.description}</p>
                                                        {  item.status === "Pending" ? (
                                                            <React.Fragment>
                                                                <CButton
                                                                    size="sm"
                                                                    color="info"
                                                                    onClick={()=>{this.approveLeave(item.leaveId, index)}}>
                                                                    Approve
                                                                </CButton>
                                                                <CButton
                                                                    size="sm"
                                                                    color="danger"
                                                                    className="ml-1"
                                                                    onClick={()=>{this.rejectLeave(item.leaveId, index)}}>
                                                                    Reject
                                                                </CButton>
                                                                <CButton
                                                                    size="sm"
                                                                    color="success"
                                                                    className="ml-1"
                                                                    onClick={() => this.props.history.push(`/employee/` + this.props.match.params.id + "/leave-edit/" + item.leaveId)}
                                                                >
                                                                    Edit
                                                                </CButton>
                                                            </React.Fragment>): ""
                                                        }
                                                    </CCardBody>
                                                </CCollapse>
                                            )
                                        }
                                }}
                            />
                        </CCardBody>
                        <CModal
                            show={this.state.modal}
                            onClose={this.toggle}
                        >
                            <CModalHeader closeButton>Delete leave</CModalHeader>
                            <CModalBody>
                                Are you sure you want to delete leave information?
                            </CModalBody>
                            <CModalFooter>
                                <CButton color="primary"
                                onClick={()=>this.deleteLeave()}
                                >Delete</CButton>{' '}
                                <CButton
                                    color="secondary"
                                    onClick={this.toggle}
                                >Cancel</CButton>
                            </CModalFooter>
                        </CModal>
                    </CCard>
                </CCol>
            </CRow>
        )
    }
}

export default Employee
