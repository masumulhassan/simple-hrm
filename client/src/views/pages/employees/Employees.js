import React from 'react'
import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable, CModal, CModalBody, CModalFooter, CModalHeader,
    CPagination,
    CRow
} from '@coreui/react'

import APICaller from "../../../utils/APICaller";
import {toast} from "react-toastify";

class Employees extends React.Component {

    constructor(props) {
        super(props);
        const queryPage = this.props.location.search.match(/currentPage=([0-9]+)/, '')
        const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
        this.state = {
            employeeList: [],
            totalPages: 1,
            currentPage: currentPage,
            tableSearchInput: "",
            modal: false,
            employeeIdToDelete: null
        }
    }

    getSearchData = (searchQuery = "", eventType = "", event) => {
            this.state.tableSearchInput = searchQuery
            this.getEmployeeList(1, searchQuery)
    }

    deleteEmployee = () => {
        if (this.state.employeeIdToDelete !== null) {
            new APICaller().DELETE("api/v1/employee/" + this.state.employeeIdToDelete, (response) => {
                toast.success("Employee deleted successfully")
                this.getEmployeeList(this.state.currentPage, this.state.tableSearchInput)
            }, (response) => {
                console.log(response)
            })
        }
        this.setState({modal: false, employeeIdToDelete: null})
    }

    toggle = (action) => {
        if(action === "open") {
            this.setState({modal: true})
        } else {
            this.setState({modal: false, employeeIdToDelete: null})
        }
    }

    getPageData = (pageNumber = 1, isActivePageChange = false) => {
        if(isActivePageChange) {
            //Discard
        } else {
            if(this.state.tableSearchInput !== ""){
                this.getEmployeeList(pageNumber, this.state.tableSearchInput)
            } else {
                this.getEmployeeList(pageNumber)
            }
        }
    }

    getEmployeeList = (pageNumber = 1, searchQuery = "") => {
        let apiUrl = "";
        if(searchQuery === "") {
            apiUrl = "api/v1/employee?currentPage=" + pageNumber;
        } else {
            apiUrl = "api/v1/search?currentPage=" + pageNumber + (searchQuery === "" ? "" : "&q=" + searchQuery);
        }
        new APICaller().GET(apiUrl , (response) => {
          this.setState({
              employeeList: response.data.employees instanceof Array ? response.data.employees : [],
              totalPages: response.data.totalPages,
              currentPage: response.data.totalPages < pageNumber ? response.data.totalPages : pageNumber,
          })
        }, (response) => {
            console.log(response)
        })
    }

    componentDidMount() {
      this.getEmployeeList()
    }

    render() {
        const itemList = this.state.employeeList;
        return(
            <CRow>
                <CCol xl={12}>
                    <CCard>
                        <CCardHeader>
                            Employees
                        </CCardHeader>
                        <CCardBody>
                            <div className="row my-2 mx-0">
                                <div className="col-xl-10 form-inline">
                                    <>
                                        <label className="mr-2">Search</label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            style={{width:"calc(100vh - 100px)"}}
                                            placeholder="name, email, department, designation"
                                            onChange={(e) => {
                                                this.getSearchData(e.target.value)
                                            }}
                                            onInput={(e) => {
                                                this.getSearchData(e.target.value, "input", e)
                                            }}
                                            value={this.state.tableSearchInput || ''}
                                        />

                                    </>
                                </div>
                                <div className="col-xl-2 form-inline" align="right">
                                    <CButton
                                        color="success"
                                        variant="outline"
                                        shape="square"
                                        size="sm"
                                        onClick={() => this.props.history.push(`/employee-add`)}
                                    >
                                        Add Employee
                                    </CButton>
                                </div>
                            </div>
                            <CDataTable
                                items={itemList}
                                fields={[
                                    {key: 'name', _classes: 'font-weight-bold'}, 'email',
                                    'joiningDate', 'designation', 'department', 'actions'
                                ]}
                                hover
                                striped
                                itemsPerPage={12}
                                scopedSlots = {{
                                    'actions':
                                        (item, index) => {
                                            return (
                                                <td className="py-2">
                                                    <CButtonGroup>
                                                        <CButton
                                                            color="success"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => this.props.history.push(`/employee/${item.employeeId}`)}
                                                        >
                                                            Details
                                                        </CButton>
                                                        <CButton
                                                            color="info"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={() => this.props.history.push(`/employee-edit/${item.employeeId}`)}
                                                        >
                                                            Edit
                                                        </CButton>
                                                        <CButton
                                                            color="danger"
                                                            variant="outline"
                                                            shape="square"
                                                            size="sm"
                                                            onClick={()=>{
                                                                this.state.employeeIdToDelete = item.employeeId;
                                                                this.toggle("open")}
                                                            }
                                                        >Delete</CButton>
                                                    </CButtonGroup>
                                                </td>
                                            )
                                        },
                                }}
                            />
                            <CPagination
                                activePage={this.state.currentPage}
                                onActivePageChange={this.getPageData}
                                pages={this.state.totalPages}
                                doubleArrows={false}
                                align="center"
                            />
                        </CCardBody>
                        <CModal
                            show={this.state.modal}
                            onClose={this.toggle}
                        >
                            <CModalHeader closeButton>Delete employee</CModalHeader>
                            <CModalBody>
                                Are you sure you want to delete employee information?
                            </CModalBody>
                            <CModalFooter>
                                <CButton color="primary"
                                         onClick={()=>this.deleteEmployee()}
                                >Delete</CButton>{' '}
                                <CButton
                                    color="secondary"
                                    onClick={this.toggle}
                                >Cancel</CButton>
                            </CModalFooter>
                        </CModal>
                    </CCard>
                </CCol>
            </CRow>
        )
    }
}

export default Employees
