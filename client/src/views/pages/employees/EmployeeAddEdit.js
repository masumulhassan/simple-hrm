import React from 'react'
import {
    CButton, CButtonGroup,
    CCard,
    CCardBody, CCardHeader,
    CCol,
    CContainer,
    CFormGroup, CFormText, CInput, CLabel,
    CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import APICaller from "../../../utils/APICaller";
import {toast} from "react-toastify";
import AppUtil from "../../../utils/AppUtil";

class EmployeeAddEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employee: {
                firstName: "",
                middleName: "",
                lastName: "",
                email: "",
                dateOfBirth: "",
                joiningDate: "",
                department: "",
                designation: ""
            }
        }
    }

    getEmployeeDetails = () => {
        new APICaller().GET("api/v1/employee/" + this.props.match.params.id, (response) => {
            delete response.data.leaves;
            this.setState({
                employee: AppUtil.clean(response.data)
            })
        }, (response) => {
            this.setState({"employeeDetails": [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]})
        })
    }

    submitEmployeeAddEditForm = (event) => {
        event.preventDefault()
        if(this.props.match.params.id !== undefined){
            new APICaller().PATCH("api/v1/employee/" + this.props.match.params.id, this.state.employee, (response) => {
                toast.success("Employee information updated successfully")
                this.props.history.push("/")
            }, (response) => {
                response.data.errors.forEach((value => {
                    toast.error(value.message)
                }))
            })
        } else {
            new APICaller().POST("api/v1/employee", this.state.employee, (response) => {
                toast.success("Employee information added successfully")
                this.props.history.push("/")
            }, (response) => {
                response.data.errors.forEach((value => {
                    toast.error(value.message)
                }))
            })
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            this.getEmployeeDetails()
        }
    }

    render() {
        return (
            <CContainer fluid>
                <CRow>
                    <CCol xs="12" md="6">
                        <CCard>
                            <CCardHeader>
                                Employee
                            </CCardHeader>
                            <CCardBody>
                                <form className="form-horizontal" onSubmit={this.submitEmployeeAddEditForm}>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="first-name-input">First name</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                id="first-name-input"
                                                name="first-name-input"
                                                required
                                                placeholder="Enter employee's first name"
                                                value={this.state.employee.firstName}
                                                onChange={(e)=>{
                                                    this.state.employee.firstName = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="middle-name-input">Middle name</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                id="middle-name-input"
                                                name="middle-name-input"
                                                placeholder="Enter employee's middle name"
                                                value={this.state.employee.middleName}
                                                onChange={(e)=>{
                                                    this.state.employee.middleName = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="last-name-input">Last name</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                id="last-name-input"
                                                name="last-name-input"
                                                placeholder="Enter employee's last name"
                                                value={this.state.employee.lastName}
                                                onChange={(e)=>{
                                                    this.state.employee.lastName = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="email-input">Email</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                type="email"
                                                id="email-input"
                                                name="email-input"
                                                placeholder="Enter Email"
                                                autoComplete="email"
                                                required
                                                value={this.state.employee.email}
                                                onChange={(e)=>{
                                                    this.state.employee.email = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                            <CFormText className="help-block">Please enter employee's email.</CFormText>
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="dob">Date of birth</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                type="date"
                                                id="dob"
                                                name="dob"
                                                placeholder="date"
                                                required
                                                value={this.state.employee.dateOfBirth}
                                                onChange={(e)=>{
                                                    this.state.employee.dateOfBirth = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="joining-date">Joining date</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                type="date"
                                                id="joining-date"
                                                name="joining-date-input"
                                                placeholder="date"
                                                required
                                                value={this.state.employee.joiningDate}
                                                onChange={(e)=>{
                                                    this.state.employee.joiningDate = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="department">Department</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                id="department"
                                                name="department-input"
                                                placeholder="Enter employee's department name"
                                                required
                                                value={this.state.employee.department}
                                                onChange={(e)=>{
                                                    this.state.employee.department = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="3">
                                            <CLabel htmlFor="designation">Designation</CLabel>
                                        </CCol>
                                        <CCol xs="12" md="9">
                                            <CInput
                                                id="designation"
                                                name="designation-input"
                                                placeholder="Enter employee's job title / designation"
                                                required
                                                value={this.state.employee.designation}
                                                onChange={(e)=>{
                                                    this.state.employee.designation = e.target.value;
                                                    this.setState({employee: this.state.employee})
                                                }}
                                            />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol md="12" align="center">
                                            <CButtonGroup>
                                                <CButton
                                                    type="submit"
                                                    color="info"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                >Submit</CButton>
                                                <CButton
                                                    color="danger"
                                                    variant="outline"
                                                    shape="square"
                                                    size="sm"
                                                    onClick={()=>this.props.history.push("/")}
                                                >Cancel</CButton>
                                            </CButtonGroup>
                                        </CCol>
                                    </CFormGroup>
                                </form>
                            </CCardBody>
                        </CCard>
                    </CCol>
                </CRow>
            </CContainer>
        )
    }
}

export default EmployeeAddEdit
