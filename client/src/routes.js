import React from 'react';
import EmployeeAddEdit from "./views/pages/employees/EmployeeAddEdit";
import Employees from "./views/pages/employees/Employees";
import Employee from "./views/pages/employees/Employee";
import LeaveAddEdit from "./views/pages/leaves/LeaveAddEdit";


const routes = [
  { path: '/', exact: true, name: 'Employees', component: Employees },
  { path: '/employees', exact: true,  name: 'Employees', component: Employees },
  { path: '/employee/:id', exact: true, name: 'Employee details', component: Employee },
  { path: '/employee-add', exact: true, name: 'Employee add', component: EmployeeAddEdit },
  { path: '/employee-edit/:id', exact: true, name: 'Employee edit', component: EmployeeAddEdit },
  { path: '/employee/:id/leave-add', exact: true, name: 'Leave add', component: LeaveAddEdit },
  { path: '/employee/:id/leave-edit/:leaveId', exact: true, name: 'Leave edit', component: LeaveAddEdit }
];

export default routes;
